import numpy as np;

def rho(param, x, psi_T, psi2):
    """
    Evaluates the distribution function rho for a trial wave function
    
    Parameters:
        param (array): parameters for trial wave function
        x: coordinates
        psi_T: trial wave function
        psi2: <psi|psi>
        
        Returns:
            rho(x): probability density at x
    """
    Psi = psi_T(param,x);
    return np.conj(Psi)*Psi/psi2(param);

def metStep(r1, rho, stepSize):
    """
    Performs a step in the Metropolis Walk according to the desired
    distribution. A uniform distribution is used to make a step.
    
    Parameters:
        r1 (array): coordinate array
        rho (function): desired distribution, weight in integral
        stepSize: maximum step a walker can take
        
        Returns:
            r2: new coordinate array
    """
    r2 = r1 + stepSize*2*(np.random.rand(*r1.shape)-0.5); #Proposed coordinate
    p = rho(r2)/rho(r1);
    if p<1:
        if np.random.rand()<p:
            return r2;
        else:
            return r1;
    else:
        return r2;

def metWalk(f, rho, r, param, stepSize, Nw, Nsteps, throwAway):
    """
    Performs a Metropolis Walk according to the desired distribution.
    to calculate an integral according to Monte Carlo integration.
    A uniform distribution is used to make a step.
    
    Parameters:
        f (function): function to integrate with weight rho
        rho (function): desired distribution, weight in integral
        r: degrees of freedom
        param: parameters needed for f
        stepSize: maximum step a walker can take
        Nw: number of walkers
        Nsteps: number of steps per walker (after throwAway)
        throwAway: number of steps per walker discarded
        
        Returns:
            I (float): integral
    """
    I = 0;
    Ia = 0;
    a = np.zeros(100);
    for m in range(Nsteps+throwAway):
#        print(r);
        for n in range(Nw):
            rold = r[n].copy();
            r[n] = metStep(r[n], rho, stepSize);
            if m >= throwAway:
                I += f(param,r[n]);
            else:
                if (r[n]==rold).all():
                    a[Ia] = 0;
                else:
                    a[Ia] = 1;
                Ia += 1;
                if Ia==100:
                    stepSize = np.exp( (np.mean(a)-0.234)/4 ) * stepSize;
                    Ia = 0;
#                    print("stepSize = {}".format(stepSize));
#                    print("np.mean(a) = {}".format(np.mean(a)));
                
                
    I = I/(Nw*Nsteps);
    return I;


import numpy as np;
from scipy.optimize import minimize;
import matplotlib.pyplot as plt;


# Helium atom

import VarQuaMonCar as vqmc

# Trial wave function
# Takes array of coefficients c and array (!) of spatial arguments
def psiT1_He(alpha, x):
    """
    Trial wave function for the helium atom
    
    Parameters:
        alpha: parameter
        x: coordinate
        
    Returns:
        np.exp(-x**2/2)*np.dot(c,X(x, Nvec))
    """
    return np.exp(-alpha*(np.linalg.norm(x[0])+np.linalg.norm(x[1])));

# analytical solution for <psi*|psi>
def psiT1_Norm_He(alpha):
    """
    Function calculating norm of trial wave function <psi|psi>
    for the helium atom.
    
    Parameters:
        alpha: parameter
        
    Returns:
        <psi|psi>
    """
    return np.pi**2/alpha**6

# Local energy
# Takes coefficient alpha and array (!) of spatial arguments
def Eloc1_He(alpha, x):
    """
    Local energy for the helium atom.
    
    Parameters:
        alpha: parameter
        x: coordinate
        
    Returns:
        (H psi)/psi
    """
    return -alpha**2 + (alpha-2)*(1/np.linalg.norm(x[0])+1/np.linalg.norm(x[1])) + 1/np.linalg.norm(x[0]-x[1]);

def calcE_He(alpha, Nw, Nsteps, throwAway):
    """
    Calculates the energy of the helium atom using Monte Carlo
    integration and Metroplis Walk.
    
    Parameters:
        alpha: parameter
        Nw: number of walkers
        Nsteps: number of steps per walker (after throwAway)
        throwAway: number of steps per walker discarded
        
    Returns:
        E: energy integral
    """
    
    def rho2(x):
        return vqmc.rho(alpha, x, psiT1_He, psiT1_Norm_He);
    
    lim = 4/alpha;
    r0 = lim*2*(np.random.rand(Nw,2,3)-0.5);    # Initialise walkers between -lim and lim
    E = vqmc.metWalk(Eloc1_He, rho2, r0, alpha, 0.02, Nw, Nsteps, throwAway);
    
    print('alpha = {}'.format(alpha));
    print('E = {}'.format(E));
    return E;

def calcE_He2(alpha):
    """
    Calculates the energy of the helium atom using Monte Carlo
    integration and Metroplis Walk with certain optimal parameters.
    
    Parameters:
        alpha: parameter
        
    Returns:
        E: energy integral
    """
    return calcE_He(alpha, 20, 10000, 320);

# Numerical solution for gradient
def gradE1_He(alpha):
    """
    Calculates the gradient of the energy of the helium atom
    by central finite difference with certain optimal parameters.
    The difference of alpha is taken 2*0.1
    
    Parameters:
        alpha: parameter
        
    Returns:
        grad E: gradient of energy integral
    """
    dalpha = 0.1;
    return (calcE_He2(alpha+dalpha)-calcE_He2(alpha-dalpha))/(2*dalpha);

# Try some coefficient
alpha = 5;

# Do calculation for E
# Ground state energy is -2.902, minimum for psi^T_1 is 2.844
E = calcE_He(alpha, 20, 10000, 320);

# Minimise
optRes = minimize(calcE_He2, jac=gradE1_He, x0=alpha, tol=1e-1);
print(optRes);





# check integrals

#Nres = 1000000
#I = 0;
#lim = 2;
#for n in range(Nres):
#    x = lim*2*(np.random.rand(2,3)-0.5);
#    Psi = psiT1_He(alpha,x);
#    I += np.conj(Psi)*Psi;
#    if n%10000==0:
#        print(I/n*(2*lim)**6);
#print(psiT1_Norm_He(alpha))
        
#Nres = 1000000
#E = 0;
#lim = 4;
#for n in range(Nres):
#    x = lim*2*(np.random.rand(2,3)-0.5);
#    E += rho(alpha, x, psiT1_He, psiT1_Norm_He)*Eloc1_He(alpha, x);
#    if n%10000==0:
#        print(E/n*(2*lim)**6);

## Make sigma-throwAway plot
#Nres = 10;
#eTA = np.linspace(1, 4, Nres);
#Nsamp = 40;
#E = np.zeros(Nsamp);
####sigma = np.zeros(Nres);
#for m in range(Nres):
#    print("m = {}".format(m));
#    if sigma[m]==0:
#        TA = int(10**eTA[m]);
#        for n in range(Nsamp):
#            E[n] = calcE_He(alpha, 10, 3000, TA);
#        sigma[m] = np.std(E);
#        print(sigma[m]);
#        print();
#np.save('sigma_He', sigma);
#np.save('eTA_He', eTA);

\documentclass{llncs}
\usepackage{braket}
\usepackage{todonotes}
\usepackage{listings}
\usepackage{float}
\usepackage{llncsdoc}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage{changepage}
\usepackage{caption,subcaption}
\usepackage[utf8]{inputenc}
\usepackage[margin=3cm]{geometry}
 \geometry{
 a4paper,
 }
\graphicspath{ {./figures/} }

\begin{document}
\begin{flushleft}
\LARGE\bfseries Computational Physics\\
 Project 2 report\\[2cm]
Variational Quantum Monte Carlo simulation\\
\end{flushleft}
\rule{\textwidth}{1pt}
\vspace{2pt}
\begin{flushright}
\Huge
\begin{tabular}{@{}l}
Victor Boogers 4009436\\ 
Theo Basili 4050134\\
{\Large \today}
\end{tabular}
\end{flushright}
\rule{\textwidth}{1pt}
\vfill
\newpage
\tableofcontents
\vfill
\section*{\textbf{Abstract}}
This report documents the results found by making a variational Monte Carlo simulation. The simulation is used in order to solve the Schrodinger equation for the Helium atom. Finding the best possible solution that, because solving it exactly is not possible for a Helium atom only for the Hydrogen atom.\\
We choose a trial wave function and use Markov chains to create a set of samples. These generated samples are then used to solve the integral for the ground state energy of Helium. This is known as Monte Carlo integration and together with metropolis walkers it is possible to create the samples.\\
A gradient descent minimization algorithm is used to calculate the minimal energy. The method is implemented for the harmonic oscillator and the Helium atom. 
\clearpage
\section{Introduction}
In the theory of quantum mechanics only certain problems can be solved analytically. To be precise, for the harmonic oscillator and the hydrogen atom, the wave function can be determined exact. This is done by solving the Schrodinger equation for these specific cases. In most cases this can not be solved analytically and can only be approximated by various methods.\\

The goal of the project is to approximate the wave function and energy of the Helium atom using a variational Monte Carlo method. The Metropolis algorithm will be implemented to obtain a desired distribution for sampling the integrand. These methods are needed in order to calculate the ground state energy. The ground state is minimized using the parameter $\alpha$ in the trial wave function. \\

The report is setup in the following way. It starts with a brief introduction into the theory of variational quantum mechanics. Then the Monte Carlo integration will be discussed. Relevant formulas to be implemented will be treated, followed by a chapter with results. At the end a conclusion is given and the group work organisation is discussed.
\clearpage
\section{Theory}
\subsection{Variational method}
As stated in the introduction, the stationary Schrödinger equation can only be solved exactly for some specific examples. In general, and certainly for interacting systems, an exact solution is impossible. Still, it is possible to find the approximate ground state energy using a variational calculation \cite{opd}.
In order to do so, a *trial wave function* is considered  $\psi_T(\mathbf{x}, \alpha)$, where $\alpha$ is one or more
variational parameters. The expectation value of the energy is

\begin{equation}
    E(\alpha) = \frac{\int d\mathbf{x}\, \psi_T^*(\mathbf{x},\alpha)\, H\, \psi_T(\mathbf{x}, \alpha)}{\int d\mathbf{x}\, \psi_T^*(\mathbf{x},\alpha) \psi_T(\mathbf{x}, \alpha)} .
\end{equation}
The variational principle tells that $E(\alpha) \geq$ the real ground state energy. The best approximation to the
ground state energy is thus obtained by minimizing $E(\alpha)$ with respect to $\alpha$. The quality
of the approximation depends on how good the trial wave function is.

The Monte Carlo method is in this case used to compute the integral over the degrees of freedom of the wave function
$\mathbf{x}$. To get a proper probability, the local energy is defined as

\begin{equation}
    E_\mathrm{loc}(\mathbf{x}, \alpha) = \frac{H \psi_T(\mathbf{x}, \alpha)}{\psi_T(\mathbf{x}, \alpha)} .
\end{equation}
Hence, the energy can be computed as 

\begin{equation}
    E(\alpha) = \frac{\int d\mathbf{x}\, \psi_T^*(\mathbf{x},\alpha)\psi_T(\mathbf{x}, \alpha)\, E_\mathrm{loc}(\mathbf{x}, \alpha)}{\int d\mathbf{x}\, \psi_T^*(\mathbf{x},\alpha) \psi_T(\mathbf{x}, \alpha)} .
\end{equation}
This integral is solved using Monte Carlo integration with importance sampling according to the probability density $\psi_T^* \psi_T\int d\mathbf{x} \psi_T^* \psi_T$.

\subsection{Monte Carlo integration}
The Monte Carlo method is a computational way to approach an integral. Integrals are estimated with the following equation. 
\begin{equation}
    \int_a^b f(x)dx = \frac{b-a}{N} \sum_{i=1}^N w_i f(x_i)
\end{equation}
This approximation has an error in the order of $\frac{1}{\sqrt{N}}$. The weighs $w_i$ can be a distribution function which can be chosen in order to distribute the samples in an efficient way. For big N is this error small, hence it is a better way to integrate. N is the number of samples. The method is typically used for calculating integrals which are not analytically solvable. Monte Carlo method is mainly used in higher order dimensional integrals. This is due to the fact that the error is independent of the dimension. The error for other integration methods is $N^{-\frac{k}{d}}$ with $k\geq1$ depending on the specific method and d the number of dimensions. Comparing this with the error of the Monte Carlo method which is $N^{-1/2}$, obviously shows that the error for the Monte Carlo method is smaller than other integration methods for higher dimensional problems. Quantum mechanics has much higher dimensional problems with
integrals, which are not analytically solvable. The Helium atom, which is considered in this project, is an example of this kind of problem.

\subsection{Markov chains}
In general in order to calculate the expected value of an operator in quantum mechanics the following equation is used,
\begin{equation}
    <A>=\frac{\int \psi^\dagger A(x) \psi dx}{\int \psi^\dagger \psi dx}
\end{equation}
where $<A>$ is the expected value of the operator A(x), $\psi$ the wave function for the given quantum state. 
Focusing on the denominator term of this equation, the product of the wave function and its conjugate is the probability density function of this state. This of course gives the chance to find a particle at a given location. This function is usually not a uniform distribution. 
An efficient way to choose the samples $x_i$, is to take the $x_i$ from a distribution which is the same as $\psi^\dagger \psi$. This results in
\begin{equation}
    \int_a^b p(x) A(x) dx \approx \frac{b-a}{N} \sum_{i=1}^N f(x_i) ,
\end{equation}
where p(x) is the distribution function, in our case this equals $\psi^\dagger \psi$. A(x) is the operator that we are calculating.
Now the task is to sample $x_i$ following this 'quantum' distribution. In order to achieve this Markov chains are used. A Markov chain makes use of random walkers that move through space while taking samples on their positions. Say that a walker is at position $x_n$ it will then propose a new position $x_{n+1}$. This new position will only be accepted when $p(x_n) < p(x_{j+1})$ and it will then be added to the chain. To prevent that the chain is stuck in a local minimum u is taken from a uniform distribution
when $p(x_n) > p(x_{n+1})$. If $u < \frac{p(x_{n+1})}{p(x_n)}$ the new position $x_{n+1}$ is accepted and added to the chain. This process makes it possible for a walker to move on even if it is in the local maximum. Repeating this process will result in a set of $x_i$ which is used to approximate integrals. 

The Metropolis algorithm makes use of a Markov chain to sample a set of $x_i$ samples. The main challenge in implementing the Metropolis
algorithm lies in the construction of the Markov chain. In order to built a Markov chanin a separate function is created, which can construct a Markov chain for any given trial wave function. The Markov chain starts with a random location for all M walkers in the d dimensional space chosen via a normal distribution. The next step is to calculate the distribution $p(x_n)$ for all walkers and then start a loop over N iterations. Each iteration starts with randomly choosing a $x_i$ from a distribution, for which $P(x_2|x_1)=P(x_1|x_2)$ (the chance of choosing $x_2$ after $x_1$ is the same as vice versa), and small relative to the size of the potential. The acceptence rate can be adjusted to be close to 0.234 during the first part of the walk that is thrown away. This is an optimal value according to literature \cite{Mwalk}.

\newpage
\subsection{Energy minimization}
The energy is minimized with respect to the parameter $\alpha$. This is not a straight forward minimization problem as the result of the Monte Carlo integration for $E(\alpha)$ has an error associated with it. 

\subsection{Harmonic oscillator}
The variational Monte Carlo method is first applied on a solvable model, the harmonic oscillator. This is done in order to test the algorithm. First the following trial wave function is chosen
\begin{equation}
    \psi_T = \sum_{p=0}^{N} c_p e^{-{x^2}/2} x^p ,
\end{equation}
where $c_p$ are the variation parameters $\alpha$. With this we should be able to reproduce the eigenstates and so also the ground state, which are products of the above gaussian function with Hermite polynomials. The operator $H=\frac{1}{2}(x^2 - \frac{\partial^2}{\partial x^2})$ works on the trial wave function in order to find local energy. The expression for the local energy for this trial wave function is derived to be

\begin{equation}
        E_\mathrm{loc}(\mathbf{x}, c_p) = - \frac{\sum_{p=0}^{N} [c_{p-2}+(p+2)(p+1)]x^p}{2\sum_{p=0}^{N} c_p x^p}.
\end{equation}
To find the gradient of the energy without having to compute the difficult integral for energy many times, we analytically take the derivatives.

\begin{equation}
    \centering
    \nabla_\alpha \braket{E} = \nabla_\alpha\frac{\braket{\psi|H|\psi}}{\braket{\psi|\psi}} = &\frac{\int[(\nabla_\alpha \psi)(H\psi)+\psi(\nabla_\alpha H \psi)]dx}{\braket{\psi|\psi}^2} -\frac{\braket{\psi|H|\psi}\int \nabla_\alpha \psi^2 dx}{\braket{\psi|\psi}}
\end{equation}
In the case of the harmonic oscillator this reads

\begin{equation}
    \left[\nabla_\alpha \braket{E}\right]_p = \frac{\int\varphi_p . H \psi +\psi \varphi^{'}_p dx}{\braket{\psi|\psi}}-\frac{2E\int \varphi_p\psi dx}{\braket{\psi|\psi}}
\end{equation}
with

\begin{align}
    \varphi_p H \psi &= \frac{\sum_{n=0}^N c_n e^{-x^2} [-(2n+1)x^{n+p} + n(n-1) x^{n+p-2}]}{2}, \\
    \psi \varphi_p &= \frac{\sum_{n=0}^N c_n e^{-x^2} [-(2p+1)x^{n+p} + p(p-1) x^{n+p-2}]}{2}, \\
    \varphi_p \psi &= \sum_{n=0}^N c_n e^{-x^2} x^{n+p},
\end{align}
Hence,

\begin{multline}
    \left[\nabla_\alpha \braket{E}\right]_p = \sum_{n=0}^N c_n \frac{((1+(-1^{n+p})))}{2} \\
    .\frac{-(2n+2p+2-4E) \Gamma (\frac{n+p+1}{2}) +p(p-1+n(n-1)) \Gamma (\frac{n+p-1}{2})}{2\braket{\psi|\psi}}
\end{multline}


\newpage
\subsection{Helium atom}
The Hamiltonian for the helium atom is
\begin{equation}
\begin{split}
     \centering
     H &= -\frac{\hbar^2}{2m_e}(\nabla_1^2+\nabla_2^2) +\frac{e^2}{4\pi\epsilon_0} \left( -\frac{2}{r_1} -\frac{2}{r_2} +\frac{1}{|\vec r_1-\vec r_2|} \right) \\
     &= -\frac{\hbar^2}{2m_e}(\nabla_1^2+\nabla_2^2) +\frac{\hbar^2}{a_0m_e} \left( -\frac{2}{r_1} -\frac{2}{r_2} +\frac{1}{|\vec r_1-\vec r_2|} \right).
\end{split}
\end{equation}
We can put this in dimensionless form
\begin{equation}
    H = -\frac{1}{2}(\nabla_1^2+\nabla_2^2) +\left( -\frac{2}{r_1} -\frac{2}{r_2} +\frac{1}{|\vec r_1-\vec r_2|} \right) ,
\end{equation}
where the distances are in units of the Bohr radius ($a_0=\frac{4\pi\epsilon_0\hbar^2}{m_{\mathrm{e}}e^2}$) and energy is in Hartrees ($E_H=\frac{\hbar^2}{m_ea_0^2}=\frac{m_ee^4}{4\epsilon_0^2h^2}=2\mathrm{Ry}$). This is just the sum of two hydrogen Hamiltonians plus the last interaction term. Therefore we choose a trial wave function similar to the hydrogen wave function.

\begin{equation}
    \psi^T = e^{-\alpha(r_1+r_2)}
\end{equation}
It is known that the energy can be approximated in this form up to 0.02\%. We calculate the local energy with this. For this it must first be noticed that

\begin{equation}
\begin{split}
    \centering
    \nabla_1^2\psi &= \frac{1}{r_1^2} \frac{\partial}{\partial r_1} r_1^2 \frac{\partial}{\partial r_1} e^{-\alpha(r_1+r_2)} \\
    &= -\alpha\frac{1}{r_1^2} \frac{\partial}{\partial r_1} r_1^2 e^{-\alpha(r_1+r_2)} \\
    &= -\alpha\frac{1}{r_1^2} (-\alpha r_1^2 e^{-\alpha(r_1+r_2)} +2r_1 e^{-\alpha(r_1+r_2)}) \\
    &= \left(\alpha^2 -\frac{2\alpha}{r_1} \right) \psi .
\end{split}
\end{equation}
Now calculating the local energy is straightforward.

\begin{equation}
\begin{split}
    E_{\mathrm{loc}} &= \frac{H\psi}{\psi} = -\frac{(\nabla_1^2+\nabla_2^2)\psi}{2\psi} -\frac{2}{r_1} -\frac{2}{r_2} +\frac{1}{|\vec r_1-\vec r_2|} \\
    &= -\alpha^2 +\frac{\alpha-2}{r_1} +\frac{\alpha-2}{r_2} +\frac{1}{|\vec r_1-\vec r_2|}
\end{split}
\end{equation}
We can also analytically find the norm of the wave function

\begin{equation}
\begin{split}
    \centering
    \braket{\psi^T|\psi^T} &= (4\pi)^2\int\int{r_1^2r_2^2e^{-2\alpha(r_1+r_2)}dr_1dr_2} \\
    &= (4\pi)^2\left(\int{r_1^2e^{-2\alpha r_1} dr_1} \right)^2 \\
    &= (4\pi)^2\left(-\frac{2}{8\alpha^3} \right)^2 = \frac{\pi^2}{\alpha^6} .
\end{split}
\end{equation}

The gradient of the energy w.r.t. our parameter is not very easily solved analytically, so we have have approximated this by simple centered finite difference using a difference of $0.1$ for $\alpha$.

\newpage
\section{Computational results}
The theoretical knowledge used in this project is provided by the book of Prof. J. Thijssen ref.\cite{jos}\cite{joss}\cite{int}. We have followed the instructions given in the assignment ref.\cite{opd}. For a more detailed presentation of the results, we refer to our simulation ref.\cite{code}. Which is uploaded to gitLab.

\subsection{Harmonic oscillator}

The integrals for $\braket{\psi^*|\psi}$ and $\braket{E}$ have been checked by simple and uniform integration and give very similar results. The energies for the lowest few states are reproduced by the simulation (results?).

We have decided to throw away the first 1000 steps of every walker considering figure \ref{fig:sigma_HO} to ensure good or fast convergence. The number of processed steps is 3000. 

The BFGS algorithm is used to minimise. The parameters converge to the ground state values and the energy becomes almost exactly 0.5, using a tolerance of $\epsilon=10^{-3}$ for the algorithm. For instance if we start at $\vec c=\begin{bmatrix}1 && 1 && 1 && 1 && 1 \end{bmatrix}$ we arrive at $\vec c=\begin{bmatrix} 1 && -3.4 && 1 && 1 && 1 \end{bmatrix}$ within 13 iterations involving 17 evaluations of both the function and the Jacobian. However, we also get a different state with parameters $\vec c=\begin{bmatrix} -2.1 && 12 &&  3.25 && 17 && -1.95\end{bmatrix}$ at an energy of approximately 2 starting with $\vec c=\begin{bmatrix}-2 && 0.2 && 4 && 0 && 0 \end{bmatrix}$. To be clear, the correct result has any value for the first coefficient and the others zero. These values also do not correspond to a different eigenstate.


\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth/2]{figures/sigma_HO.pdf}
    \caption{Standard deviation of E for the harmonic oscillator after 40 calculations as function of the logarithm of the number of steps thrown away per walker}
    \label{fig:sigma_HO}
\end{figure}

\subsection{Helium atom}

Also for this system the integrals for $\braket{\psi^*|\psi}$ and $\braket{E}$ have been checked by simple and uniform integration and also give very similar results.
We have decided to throw away the first 320 steps of every walker considering figure \ref{fig:sigma_He} to ensure good or fast convergence. The number of processed steps is 10000. 
Two analytic literature values of the energy for $\alpha=2$ and $\alpha=2-5/16$ (minimum) have been approximated ref.\cite{lit}. For the first value we get $E=-2.760$ instead of $E=-2.749$ and for the second $E=-2.8528$ instead of $E=-2.844$.

The BFGS algorithm is used to minimise. Convergence to the known minimum is achieved to reasonable degree: within a relative tolerance of $\epsilon=10^{-1}$. Starting from $\alpha=3$ the end result is $\alpha=1.7213,E=-2.865$ after 2 iterations and 4 function and 4 Jacobian evaluations, whereas the correct values are $\alpha=1.6875, E=-2.844$. For $\alpha=5$ twice as many iterations are needed.


\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth/2]{figures/sigma_He.pdf}
    \caption{Standard deviation of E for helium atom after 40 calculations as function of the logarithm of the number of steps thrown away per walker}
    \label{fig:sigma_He}
\end{figure}

\newpage
\section{Conclusions}
In the results chapter of this report there is enough evidence that the code is inline with the theoretical knowledge ref.\cite{jos},\cite{joss}. We do not know why for some initial parameters the parameters of the ground state of the harmonic oscillator were not reproduced, but perhaps this is due to the stochastic behaviour of the calculation of the gradient and maybe a too flat energy surface in the parameter space. Better understanding of the BFGS algorithm might shine light on this. The minimisation fails because of "precision loss" in the algorithm. Another thing one could try is see how changing the chain length in the Metropolis Walk effects this convergence. Perhaps at higher precision for the energy (longer chains) convergence is achieved.
\newline 

\subsection*{Code performance}
The code runs on a reasonable time scale. Results do not appear immediate, but it is only a matter of minutes. During this project numerous things have been though of in order to make the code run as efficient as possible. One thing which could be improved is a analytical expression for the energy gradient for the helium atom, as we already have for the harmonic oscillator. The code would go faster and be more precise.

\subsection*{Work organization}
\subsection*{Coding}
During the project for the computational physics course, both of the students participating worked on the Python simulation. They have acquired a lot of new knowledge and worked many hours on the coding part of the project.
Victor worked more on the coding part for this project. We had also some collaboration sessions at the university where we exchanged ideas and worked on the code. Both of the students have come up with good ideas in order to solve problems met during the project.
\subsection*{Reporting}
The reporting part of this project is done by the two students. Theo focused more on the theoretical part of the report and Victor included the results. We discussed each others work to stay up to date about the whole project. Victor essentially created the code and Theo corrected it, if he saw any errors and often when there were bugs, and he determined parameters for the code and made it generate the results. The derivations We had a good collaboration and the workload is evenly distributed.

We have tried to keep our gitLab repository up to date. At the start of the project we encountered some problems though.

\newpage 
\begin{thebibliography}{9}
\bibitem{jos} 
   Jos Thijssen: \textit{Computational Physics},
   Cambridge University Press
   2nd edition, 2013.
\bibitem{joss} 
   Jos Thijssen: \textit{Lecture notes advanced quantum mechanics} 2013.
\bibitem{int} 
   Michael Wimmer: Project 2: intro Monte Carlo simulations:
    \textbf{\url{https://gitlab.kwant-project.org/computational_physics_19/course_notes/blob/master/project\%202/montecarlo_intro.pdf}},
    \today.
\bibitem{opd} 
    Anton Akhmerov: Project 2, Variational Quantum Monte Carlo:
    \textbf{\url{https://gitlab.kwant-project.org/computational_physics_19/course_notes/blob/master/project\%202/projects.md}},
    \today.
\bibitem{code} 
    Simulation code by Theo Basili \& Victor Boogers, Delft University of Technology:
    \textbf{\url{https://gitlab.kwant-project.org/vbgetsmail/varquamoncar/blob/master/VarQuaMonCar.py}},
    \today.
\bibitem{Mwalk} 
    Chris Sherlock, Gareth Roberts, \textit{Optimal scaling of the random walk Metropolis on elliptically symmetric unimodal targets}, Bernoulli 2009, Vol. 15, No. 3, 774-798, 2009
\bibitem{lit} 
    Richard Fitzpatrick, \textit{http://farside.ph.utexas.edu/teaching/qmech/Quantum/node128.html}, July 20, 2010.
\end{thebibliography}
\end{document}
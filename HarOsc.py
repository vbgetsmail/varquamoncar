import numpy as np;
from scipy.optimize import minimize;
import matplotlib.pyplot as plt;

from math import factorial;
from scipy.special import gamma


# Harmonic Oscillator

import VarQuaMonCar as vqmc

# Number of basis vectors minus one
Nvec = 4;
# Calculate coefficients for Hermite polynomials
coef = np.zeros([Nvec+1,Nvec+1]);
coef[0,0] = 1;
coef[1,1] = 2;
for m in range(2,Nvec+1):
    coef[m,:] = 2*np.append(0,coef[m-1,0:-1])-2*(m-1)*coef[m-2,:];


def X(x, N):
    """
    Function to generate an array of powers of x up until Nth power.
    """
    return [x**n for n in range(N+1)];

# Trial wave function
# Takes array of coefficients c and array (!) of spatial arguments
def psi_T_HO(c, x):
    """
    Trial wave function for the harmonic oscillator.
    
    Parameters:
        c (array): parameters/coefficients
        x: coordinate
        
    Returns:
        np.exp(-x**2/2)*np.dot(c,X(x, Nvec))
    """
    x = x[0];    # go out of array
    return np.exp(-x**2/2)*np.dot(c,X(x, Nvec));

# analytical solution for 
def psi2_HO(c):
    """
    Function calculating norm of trial wave function <psi|psi>
    for the harmonic oscillator.
    
    Parameters:
        c (array): parameters/coefficients
        
    Returns:
        <psi|psi>
    """
    C = np.zeros(2*Nvec+1);
    for n in range(2*Nvec+1):
        for m in range(max(0,n-Nvec), min(n,Nvec)+1):
            C[n] += c[m]*c[n-m];
    return np.dot(C,[ (gamma((n+1)/2) if n%2==0 else 0) for n in range(2*Nvec+1) ]);

# Local energy
# Takes array of coefficients c and array (!) of spatial arguments
def E_loc_HO(c, x):
    """
    Local energy for the harmonic oscillator.
    
    Parameters:
        c (array): parameters/coefficients
        x: coordinate
        
    Returns:
        (H psi)/psi
    """
    x = x[0];    # go out of array
    p = np.array(range(0,Nvec+1));
    Xp = X(x,Nvec+2);
    Hpsi = np.dot(c, ((2*p+1)*Xp[:-2] - p*(p-1)*np.append([0,0],Xp[:-4]))/2);
    Eloc = Hpsi/np.dot(c,Xp[:-2]);
    return Eloc;

Nw = 20;    # number of walkers
def calcE_HO(c, Nw, Nsteps, throwAway):
    """
    Calculates the energy of the harmonic oscillator using Monte Carlo
    integration and Metroplis Walk.
    
    Parameters:
        c (array): parameters/coefficients
        Nw: number of walkers
        Nsteps: number of steps per walker (after throwAway)
        throwAway: number of steps per walker discarded
        
    Returns:
        E: energy integral
    """
    
    def rho2(x):
        return vqmc.rho(c,x,psi_T_HO,psi2_HO);
    
    lim = 5;
    r0 = lim*2*(np.random.rand(Nw,1)-0.5);    # Initialise walkers between -lim and lim
    E = vqmc.metWalk(E_loc_HO, rho2, r0, c, 0.002, Nw, Nsteps, throwAway);
    
    print('c = {}'.format(c));
    print('E = {}'.format(E));
    return E;

def calcE_HO2(c):
    """
    Calculates the energy of the harmonic oscillator using Monte Carlo
    integration and Metroplis Walk with certain optimal parameters.
    
    Parameters:
        c (array): parameters/coefficients
        
    Returns:
        E: energy integral
    """
    return calcE_HO(c, 20, 3000, 1000);

# analytical solution for divergence
def gradE_HO(c):
    """
    Calculates the gradient of the energy of the harmonic oscillator
    with certain optimal parameters.
    
    Parameters:
        c (array): parameters/coefficients
        
    Returns:
        grad E: gradient of energy integral
    """
    grad = np.zeros(Nvec+1);
    E = calcE_HO2(c);
    for p in range(Nvec+1):
        D1 = np.array([(2*n+2*p+2)/2-2*E for n in range(Nvec+1)]);
        D2 = np.array([-p*(p-1)/2 for n in range(Nvec+1)]);
        D3 = np.array([-n*(n-1)/2 for n in range(Nvec+1)]);
        Gam1 = [gamma((n+p+1)/2)*((n+p+1)%2) for n in range(Nvec+1)];
        Gam2 = [(gamma((n+p-1)/2)*((n+p+1)%2) if p>=2 else 0) for n in range(Nvec+1)];
        Gam3 = [(gamma((n+p-1)/2)*((n+p+1)%2) if n>=2 else 0) for n in range(Nvec+1)];
        grad[p] = np.dot(c,(D1*Gam1+D2*Gam2+D3*Gam3));
    return grad/psi2_HO(c);

# Try some coefficients
#c = 0.0*coef[0,:]+0.1*coef[1,:]+1.0*coef[2,:];
c = [1.0, 1.0, 1.0, 1.0, 1.0];
#c = [2.46781267, 0.0523993, 0.29774251, -0.01136095, -0.03005259];

# Do calculation for E
E = calcE_HO(c, Nw, 3000, 1000);
print('E = {}'.format(E));

# Minimise
optRes = minimize(calcE_HO2, jac=gradE_HO, x0=c, tol=1e-3);
print(optRes);




## check if integral goes to n+0.5
#E2 = 0;
#I = 0;
#lim = 8;
#h = 2*lim/Nres
#for n in range(Nres):
#    Psi = psi_T_HO(c,[(n+0.5)*h]);
#    I += np.conj(Psi)*Psi*h;
#def rho3(x):
#    Psi = psi_T_HO(c,x);
#    return np.conj(Psi)*Psi/psi2_HO(c);
#lim = 5;
#h = 2*lim/Nres;
#for n in range(Nres):
#    x = (n-Nres/2+0.5)*h;
#    E2 += rho3([x])*E_loc_HO(c,[x])*h
#print(E2);

## Make sigma-throwAway plot
#Nres = 10;
#eTA = np.linspace(1, 4, Nres);
#Nsamp = 40;
#E = np.zeros(Nsamp);
####sigma = np.zeros(Nres);
#for m in range(Nres):
#    print(m);
#    if sigma[m]==0:
#        TA = int(10**eTA[m]);
#        for n in range(Nsamp):
#            E[n] = calcE_HO(c, 30, 1000, TA);
#        sigma[m] = np.std(E);
#        print(sigma[m]);
#        print();
